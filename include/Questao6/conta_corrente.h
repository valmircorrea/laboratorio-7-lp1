/**
* @file     conta_corrente.h
* @brief    Arquivo com a classe ContaCorrente, com seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef CONTA_CORRENTE_H_
#define CONTA_CORRENTE_H_

#include "conta.h"

/**
* @class ContaCorrente. 
* @brief Classe derivada de Conta.
*/
class ContaCorrente : public Conta {
    protected:
    float saldo;            /**< Saldo da conta */
    float juros;            /**< Juros que seré aplicado na conta */
    float limite;           /**< Limite definido para controle dos saques */

    public:
        /** @brief Construtor padrao */
        ContaCorrente();

        /** @brief Metodo que incrementa o saldo */
        float setDeposito(float d);

        /** @brief Metodo que retira uma quantia da conta */
        float setSaque(float s);

        /** @brief Metodo retorna o saldo atual da conta */
        float setSaldo();

        /** @brief Metodo que retorna os juros quando o saldo esta positivo até a data atual */
        float setJurosPositivos();

        /** @brief Metodo que que contabiliza o valor dos juros quando */ 
        /*         o saldo está negativo, até a data atual, */
        float setJurosNegativos(float j);

        /** @brief Metodo que calcula e aplica os juros sobre a conta. */
        float Atualiza();

        /** @brief Metodo que define o limite para o controle de saques */
        float setLimite(float l);

        /** @brief Metodo que retorna o valor limite para o controle de saques */
        void GetLimite ();

        /** @brief Destrutor padrao */
        ~ContaCorrente();

};

#endif
/**
* @file     conta_poupanca.h
* @brief    Arquivo com a classe ContaPoupanca, com seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef CONTA_POUPANCA_H_
#define CONTA_POUPANCA_H_

#include "conta.h"
#include "data.h"

/**
* @class ContaPoupanca.
* @brief Classe ContaPoupanca derivada de Conta.
*/
class ContaPoupanca : public Conta {
    protected:
        Data aniversario;       /**< Data de aniversário do proprietário da conta/
        float saldo;            /**< Saldo da conta */
        float juros;            /**< Juros que seré aplicado na conta */

    public:
        /** @brief Construtor padrao */
        ContaPoupanca();

        /** @brief Metodo que incrementa o saldo */
        float setDeposito(float d);

        /** @brief Metodo que retira uma quantia da conta */
        float setSaque(float s);

        /** @brief Metodo retorna o saldo atual da conta */
        void setSaldo();

        /** @brief Metodo que retorna os juros quando o saldo esta positivo até a data atual */
        float setJurosPositivos();

        /** @brief Metodo que que contabiliza o valor dos juros quando */ 
        /*         o saldo está negativo, até a data atual, */
        float setJurosNegativos(float j);

        /** @brief Metodo que calcula e aplica os juros sobre a conta. */
        float Atualiza();

        /** @brief Metodo que define a data de aniversario do proprietário da conta. */
        void setAniversario (int d, int m, int a);

         /** @brief Metodo que retorna a data de aniversário do proprietário da conta. */
        Data getAniversario ();

        /** @brief Destrutor padrao */
        ~ContaPoupanca();

};

#endif
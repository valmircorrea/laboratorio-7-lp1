/**
* @file     conta.h
* @brief    Arquivo com a interface conta.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef CONTA_H_
#define CONTA_H_

/**
* @class Conta
* @brief Classe definida apenas com metodos virtuais puros.
*/
class Conta {
    public:
        virtual float setDeposito() = 0;
        virtual float setSaque() = 0;
        virtual float setSaldo() = 0;
        virtual float setJurosPositivos() = 0;
        virtual float setJurosNegativos() = 0;
        virtual float Atualiza() = 0;
};

#endif
/**
* @file     Input_iterator.cpp
* @brief    Arquivo com a função InputIterator definica com template.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef IMPUT_ITERADOR_H_
#define IMPUT_ITERADOR_H_

#include <vector>
using std::vector;

/**
* @brief Função que move o iterador até a posição do valor medio em um vetor.
* @param firt Iterador que aponta para o primeiro elemento do container.
* @param last Iterador que aponta para uma posição após o último elemento do container.
* @return it Iterador para o elemento nesse intervalo, cujo valor é o mais próximo da média do intervalo.
*/
template<typename InputIterator>
InputIterator closest2mean(InputIterator first, InputIterator last) {

    int media = 0, cont = 0;
    auto it = first;

    for (; it != last; it++ ) {
        media += *it;
        cont++;
    }

    media /= cont;

    for (it = first; it != last; it++ ) {
        if (media < *it) {
            break;
        }
    }
    
    return it;

}

#endif
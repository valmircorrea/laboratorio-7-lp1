/**
* @file     print_elements.h
* @brief    Arquivo com a função print_elements, definida com template.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef PRINT_ELEMENTS_H_
#define PRINT_ELEMENTS_H_

#include <iostream>
using std::cout;
using std::endl;

/**
* @brief Função que imprime dados de um container com labels e separadores espcíficos. 
* @param collection Parâmetro que recebe um container qualquer.
* @param label Variável que recebe uma "legenda" para a impressão.
* @param separator Variável que recebe uma string para separar os dados, no momento da impressão.
*/
template<typename TContainer>
void print_elements(const TContainer& collection, const char* label="", const char separator=' ') {
    
    auto it = collection.begin();

    cout << label;
    for (; it != collection.end(); it++ ) {
        cout << *it << separator;
    }

    cout << endl;
}

#endif
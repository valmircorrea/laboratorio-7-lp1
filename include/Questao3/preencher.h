/**
* @file     preencher.h
* @brief    Arquivo com a função preencher_stack.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef PREENCHER_H_
#define PREENCHER_H_

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <stack>
using std::stack;

#include <string>
using std::string;

/**
* @brief Função que preenche a pilha com operador e operandos. 
* @param &op Parâmetro referênciado que recebe um container do tipo stack.
* @param argc Variével que recebe a quantidade de argumentos passados pelo teminal.
* @param *argv[] Vetor de char que conte os argumentos que foram passados pelo terminal. 
*/
void preencher_stack (stack<int> &op, int argc, char *argv[]);

#endif
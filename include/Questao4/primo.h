/**
* @file     primo.h
* @brief    Arquivo com a assinatura da função primo.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef PRIMO_H_
#define PRIMO_H_

/**
* @brief Função que retorna um booleano, sinalizando se um numero é primo ou não. 
* @param ii Variável que recebe um número para ser verificado se é primo.
*/
bool primo (int ii);

#endif
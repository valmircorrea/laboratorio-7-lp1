/**
* @file     imprimir_primos.h
* @brief    Arquivo com a função para imprimir os numeros primos do vetor.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef IMPRIMIR_PRIMOS_H_
#define IMPRIMIR_PRIMOS_H_

#include <vector>
using std::vector;

/**
* @brief Função que imprime os numeros primos. 
* @param &v Parâmetro referênciado que recebe um container do tipo vector.
*/
void imprimir_primos (vector<int> &v, int n);

#endif
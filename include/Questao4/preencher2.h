/**
* @file     preencher2.h
* @brief    Arquivo com o cabeçario da função preencher.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#ifndef PREENCHER2_H_
#define PREENCHER2_H_

/**
* @brief Função que preenche a pilha com operador e operandos. 
* @param &c Parâmetro referênciado que recebe um container do tipo conjunto.
* @param n Variável que recebe a quantidade a ser preenchida no conjunto.
*/
void preencher (vector<int> &v, int n);

#endif

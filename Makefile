#Makefile for "laboratorio-06" C++ application
#Created by Valmir Correa 09/05/2017

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
DEF_DIR=./data/default

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++11

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: Questao1 Questao2 Questao3 Questao4 Questao5 Questao6

# Define o alvo para a compilação em partes:
Questao1: Questao1
Questao2: Questao2
Questao3: Questao3
Questao4: Questao4
Questao5: Questao5
Questao6: Questao6

# Debug:
debug: CFLAGS += -g -O0
debug: clean Questao1 Questao2 Questao3 Questao4 Questao5 Questao6

#-------------------------------------------------------------------------------------------------------------------------------------------
# Alvo para a contrução do executável Questao1:
Questao1: CFLAGS += -I. -I$(INC_DIR)/Questao1
Questao1: $(OBJ_DIR)/main.o  
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main.o:
$(OBJ_DIR)/main.o: $(SRC_DIR)/Questao1/main.cpp $(INC_DIR)/Questao1/input_iterator.h
	$(CC) -c $(CFLAGS) -o $@ $<

#-------------------------------------------------------------------------------------------------------------------------------------------
# Alvo para a contrução do executável Questao2:
Questao2: CFLAGS += -I. -I$(INC_DIR)/Questao2
Questao2: $(OBJ_DIR)/main2.o
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main2.o:
$(OBJ_DIR)/main2.o: $(SRC_DIR)/Questao2/main2.cpp $(INC_DIR)/Questao2/print_elements.h
	$(CC) -c $(CFLAGS) -o $@ $<


#-------------------------------------------------------------------------------------------------------------------------------------------
# Alvo para a contrução do executável Questao3:
Questao3: CFLAGS += -I. -I$(INC_DIR)/Questao3
Questao3: $(OBJ_DIR)/main3.o $(OBJ_DIR)/preencher.o
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main3.o:
$(OBJ_DIR)/main3.o: $(SRC_DIR)/Questao3/main3.cpp $(INC_DIR)/Questao3/preencher.h 
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do preencher.o:
$(OBJ_DIR)/preencher.o: $(SRC_DIR)/Questao3/preencher.cpp $(INC_DIR)/Questao3/preencher.h 
	$(CC) -c $(CFLAGS) -o $@ $<

#-------------------------------------------------------------------------------------------------------------------------------------------
# Alvo para a contrução do executável Questao4:
Questao4: CFLAGS += -I. -I$(INC_DIR)/Questao4
Questao4: $(OBJ_DIR)/main4.o $(OBJ_DIR)/preencher2.o $(OBJ_DIR)/primo.o $(OBJ_DIR)/imprimir_primos.o
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main2.o:
$(OBJ_DIR)/main4.o: $(SRC_DIR)/Questao4/main4.cpp $(INC_DIR)/Questao4/preencher2.h $(INC_DIR)/Questao4/imprimir_primos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do preencher.o:
$(OBJ_DIR)/preencher2.o: $(SRC_DIR)/Questao4/preencher2.cpp $(INC_DIR)/Questao4/preencher2.h 
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do primo.o:
$(OBJ_DIR)/primo.o: $(SRC_DIR)/Questao4/primo.cpp $(INC_DIR)/Questao4/primo.h 
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do imprimir_primos.o:
$(OBJ_DIR)/imprimir_primos.o: $(SRC_DIR)/Questao4/imprimir_primos.cpp $(INC_DIR)/Questao4/imprimir_primos.h $(INC_DIR)/Questao4/primo.h
	$(CC) -c $(CFLAGS) -o $@ $<



#-------------------------------------------------------------------------------------------------------------------------------------------
# Alvo para a contrução do executável Questao6:
Questao6: CFLAGS += -I. -I$(INC_DIR)/Questao6
Questao6: $(OBJ_DIR)/main5.o 
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main2.o:
$(OBJ_DIR)/main6.o: $(SRC_DIR)/Questao6/main6.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


#-------------------------------------------------------------------------------------------------------------------------------------------





# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes


# Alvo para a geração automatica de documentação usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g


# Alvo usado para limpar os arquivos temporarios:
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*

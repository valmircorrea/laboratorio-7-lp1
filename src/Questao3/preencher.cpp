/**
* @file     preencher.cpp
* @brief    Arquivo com a função preencher_stack.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <stack>
using std::stack;

#include <string>
using std::string;

#include <cstdlib>
#include "preencher.h"

/**
* @brief Função que preenche a pilha com os valores da expressão e calcula a medida que recebe os operadores. 
* @param &op Parâmetro referênciado que recebe um container qualquer.
* @param argc Variével que recebe a quantidade de argumentos passados pelo teminal.
* @param *argv[] Vetor de char que conte os argumentos que foram passados pelo terminal. 
*/
void preencher_stack (stack<int> &op, int argc, char *argv[]) {

    int operador1;
    int operador2;

    for (int ii = 1; ii < argc ; ii++) {
        if ( (argv[ii][0]) == 'x') {
            operador1 = op.top();
            op.pop();
            operador2 = op.top();
            op.pop();
            operador2 *= operador1;
            op.push(operador2);
        }
        else if ( (argv[ii][0]) == '-') {
            operador1 = op.top();
            op.pop();
            operador2 = op.top();
            op.pop();
            operador2 -= operador1;
            op.push(operador2);
        }
        else if ( (argv[ii][0]) == '+') {
            operador1 = op.top();
            op.pop();
            operador2 = op.top();
            op.pop();
            operador2 += operador1;
            op.push(operador2);
        }
        else if ( (argv[ii][0]) == '/') {
            operador1 = op.top();
            op.pop();
            operador2 = op.top();
            op.pop();
            operador2 /= operador1;
            op.push(operador2);
        }
        else {
            op.push( atoi(argv[ii]));
        }
            
    }

}
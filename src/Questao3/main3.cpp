/**
* @file     main3.cpp
* @brief    Arquivo com a função princípal do programa.
* @author   Silvio Sampaio.
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <stack>
using std::stack;

#include <string>
using std::string;

#include "preencher.h"

/**@brief Função principal do programa. */
int main (int argc, char *argv[]) {

    stack<int> op;
    
    preencher_stack (op, argc, argv);

    cout << op.top() << endl;
    op.pop();

    return 0;
}
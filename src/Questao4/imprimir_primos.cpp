/**
* @file     imprimir_primos.cpp
* @brief    Arquivo com a função para imprimir os numeros primos do vetor.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;

#include <algorithm> 
using std::find_if;

#include <vector>
using std::vector;

#include "imprimir_primos.h"
#include "primo.h"

/**
* @brief Função que imprime os numeros primos. 
* @param &v Parâmetro referênciado que recebe um container do tipo vector.
*/
void imprimir_primos (vector<int> &vetor, int n) {

    cout << "Numeros primos [1 - " << n << "]: "; 
    for ( vector<int>::iterator it = vetor.begin(); it != vetor.end(); it = find_if(it, vetor.end(), primo) ) {

        cout << *it << " ";
        it++;

    }
}
/**
* @file     preencher2.cpp
* @brief    Arquivo com a função preencher.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include <vector>
using std::vector;

#include "preencher2.h"

/**
* @brief Função que preenche a pilha com operador e operandos. 
* @param &c Parâmetro referênciado que recebe um container do tipo conjunto.
* @param n Variável que recebe a quantidade a ser preenchida no conjunto.
*/
void preencher (vector<int> &v, int n) {

    for (int ii = 1; ii <= n; ii++) {
        v.push_back(ii);
    }

}
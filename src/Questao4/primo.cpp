/**
* @file     primo.cpp
* @brief    Arquivo com a função booleana para retornar se um determinado numero é primo ou não.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include "primo.h"

/**
* @brief Função que retorna um booleano, sinalizando se um numero é primo ou não. 
* @param ii Variável que recebe um número para ser verificado se é primo.
*/
bool primo (int ii) {

    if (ii == 1) {
        return true;
    }

    int cont = 0;

    for (int jj = 2; jj <= ii; jj++) {
        if (ii%jj == 0 ) {
            cont++;
        }
    }

    if (cont == 1) {
        return true;
    }

    return false;
}
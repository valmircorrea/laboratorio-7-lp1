/**
* @file     main4.cpp
* @brief    Arquivo com a função princípal do programa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include <string>
using std::string;

#include <vector>
using std::vector;

#include "preencher2.h"
#include "imprimir_primos.h"

/** @brief Função princípal */
int main (int argc, char *argv[]) {

    vector<int> vetor;
    string temp = argv[argc-1];

    int n = atoi(temp.c_str());

    preencher (vetor, n);
    imprimir_primos (vetor, n);

    return 0;
}
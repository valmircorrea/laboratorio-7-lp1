/**
* @file main.cpp
* @brief Programa que opera em 2 vetores do tipo inteiro, aplicando uma função que eleva os valores dos elementos 
*       de um dos vetores ao quadrado e coloca seus resultados em outro vetor. Manipulação a qual é realizada com 
*       elementos da STL, este último, sendo especificados por meio dos comentários abaixo.
*/

#include <iostream>
using std::cout;
using std::endl;

#include <iterator>
using std::back_inserter;

#include <vector>
using std::vector;

#include <algorithm>
using std::transform;

/**
* @brief Função que recebe um numero inteiro e retorna seu valor ao quadrado.
*/
int square(int val) {
    return val * val;
}

/**@brief Função principal. */
int main(int argc, char* argv[]) {
    
    vector<int> col;    /**< Uso do container 'vector' de inteiros*/
    vector<int> col2;   /**< Uso do container 'vector' de inteiros*/
    
    for (int i = 1; i <= 9; ++i) {
        col.push_back(i);               /**< Metodo que adiciona um novo elemento ao final do vetor, */
    }                                   /* depois do último elemento atual.*/

    /** Aplica a função 'square', nos elementos do vetor 'col' e coloca seu resultado em 'col2'*/
    transform(col.begin(), col.end(), back_inserter(col2), square); /** < back_inserter, metodo que */
                                                                    /** insere novos elementos no final do vetor.*/

    /** Utilizado um iterador para percorrer o vetor*/
    for (vector<int>::iterator it = col2.begin(); it != col2.end(); ++it) {  /** < Metodos 'begin' e 'end'*/
        cout << (*it) << " ";                                                /** utilizados para ter acesso */
    }                                                                        /** ao inicio e final do vetor, */
                                                                             /** respectivamente. */
    cout << endl;
    
    return 0;
}
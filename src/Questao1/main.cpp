/**
* @file     main.cpp
* @brief    Arquivo com a função principal do programa.
* @author   Silvio Sampaio.
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include "input_iterator.h"

/**
* @brief Função principal.
*/
int main () {

    vector<int> v { 1, 2, 3, 30, 40, 50 };
    auto result = closest2mean(v.begin(), v.end());
    cout << (*result) << endl;

    return 0;
}
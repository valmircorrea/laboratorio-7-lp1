/**
* @file     main5.cpp
* @brief    Arquivo com a função princípal do programa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

/**@brief Função principal do programa. */
int main () {



    return 0;
}
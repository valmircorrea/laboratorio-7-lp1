/**
* @file     conta_corrente.cpp
* @brief    Arquivo com as implementações dos metodos da classe ContaCorrente.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include "conta_corrente.h"
#include "conta.h"

/** @brief Construtor padrao. */
ContaCorrente::ContaCorrente() {
    saldo = 0;
    juros = 0;
    limite = 0;
}

/** 
* @brief Metodo que incrementa o saldo. 
* @param d Variável que recebe o valor a ser incrementado ao saldo.
*/
float ContaCorrente::setDeposito(float d) {
    saldo += d;
}

/** 
* @brief Metodo que retira uma quantia da conta.
* @param s Variável que recebe o valor a ser decrementado ao saldo.
*/
float ContaCorrente::setSaque(float s) {
    saldo -= s;
}

/** 
* @brief Metodo retorna o saldo atual da conta.
* @return saldo da conta.
*/
void ContaCorrente::setSaldo() {
    return saldo;
}

/** 
* @brief Metodo que retorna os juros quando o saldo esta positivo até a data atual 
* @return juros da conta, se o saldo > 0, ou 0, se não.
*/
float ContaCorrente::setJurosPositivos() {
    if (saldo >= 0) {
        return juros;
    }
    return 0;
}

/** 
* @brief Metodo que que contabiliza o valor dos juros quando
*         o saldo está negativo, até a data atual.
* @param j Variável qe recebe o jutos a ser contabilizado.
*/
float ContaCorrente::setJurosNegativos(float j) {
    if (saldo < 0) {
         juros += j;
    }
}

/** @brief Metodo que calcula e aplica os juros sobre a conta. */
float ContaCorrente::Atualiza() {
//atualizar o juros.
}

/** 
* @brief Metodo que define o limite para o controle de saques.
* @param l Variável que recebe o valor limite.
*/
float setLimite(float l) {
    limite = l;
}

/** 
* @brief Metodo que retorna o valor limite para o controle de saques.
* @return limite para o controle de saques.
*/
void getLimite () {
    return limite;
}

/** @brief Destrutor padrao */
~ContaCorrente() {

}
/**
* @file     conta_poupanca.cpp
* @brief    Arquivo com as implementações dos metodos da classe ContaPoupanca.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    06/06/2017
* @date     08/06/2017
*/

#include "conta_poupanca.h"
#include "conta.h"
#include "data.h"

/** @brief Construtor padrao. */
ContaPoupanca::ContaPoupanca() {
    aniversario(0,0,0);
    saldo = 0;
    juros = 0;
}

/** 
* @brief Metodo que incrementa o saldo. 
* @param d Variável que recebe o valor a ser incrementado ao saldo.
*/
float ContaPoupanca::setDeposito(float d) {
    saldo += d;
}

/** 
* @brief Metodo que retira uma quantia da conta.
* @param s Variável que recebe o valor a ser decrementado ao saldo.
*/
float ContaPoupanca::setSaque(float s) {
    saldo -= s;
}

/** 
* @brief Metodo retorna o saldo atual da conta.
* @return saldo da conta.
*/
float ContaPoupanca::setSaldo() {
    return saldo;
}

/** 
* @brief Metodo que retorna os juros quando o saldo esta positivo até a data atual 
* @return juros da conta, se o saldo > 0, ou 0, se não.
*/
float ContaPoupanca::setJurosPositivos() {
    if (saldo >= 0) {
        return juros;
    }
    return 0;
}

/** 
* @brief Metodo que que contabiliza o valor dos juros quando
*         o saldo está negativo, até a data atual.
* @param j Variável qe recebe o jutos a ser contabilizado.
*/
float ContaPoupanca::setJurosNegativos(float j) {
    if (saldo < 0) {
         juros += j;
    }
}

/** @brief Metodo que calcula e aplica os juros sobre a conta. */
float ContaPoupanca::Atualiza() {
//atualizar os dados
}

 /** 
* @brief Metodo que define a data de aniversario do proprietário da conta.
* @param d Dia do aniversario.
* @param m Mes do aniversario.
* @param a Ano do aniversario.
*/
void setAniversario (int d, int m, int a) {
    aniversario.setDia(d);
    aniversario.setMes(m);
    aniversario.setAno(a);
}

/** 
* @brief Metodo que retorna a data de aniversário do proprietário da conta. 
* @return Data deaniversário do proprietário da conta
*/
Data getAniversario () {
    return aniversario;
}

/** @brief Destrutor padrao */
~ContaPoupanca() {
    
}
/**
* @file     main2.cpp
* @brief    Arquivo com a função princípal do programa.
* @author   Silvio Sampaio.
* @since    06/06/2017
* @date     08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <set>
using std::set;

#include "print_elements.h"

/**
* @brief Função principal.
*/
int main() {

    set<int> numeros;
    
    numeros.insert(3);
    numeros.insert(1);
    numeros.insert(4);
    numeros.insert(1);
    numeros.insert(2);
    numeros.insert(5);

    print_elements(numeros, "Set: ");
    print_elements(numeros, "CSV Set: ", ';');

    return 0;
}